// const CopyWebpackPlugin = require('copy-webpack-plugin')

const path = require('path')

const resolve = dir => {
  return path.join(__dirname, dir)
}

// 项目部署基础
// 默认情况下，我们假设你的应用将被部署在域的根目录下,
// 例如：https://www.my-app.com/
// 默认：'/'
// 如果您的应用程序部署在子路径中，则需要在这指定子路径
// 例如：https://www.foobar.com/my-app/
const BASE_URL = process.env.NODE_ENV === 'development'
  ? '/' : '/'

module.exports = {
  publicPath: BASE_URL,
  // tweak internal webpack configuration.
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  // 如果你不需要使用eslint，把lintOnSave设为false即可
  lintOnSave: false,
  chainWebpack: config => {
    config.resolve.alias
      // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set('@', resolve('src'))
      .set('_c', resolve('src/components'))
  },
  // 设为false打包时不生成.map文件
  productionSourceMap: true,
  // 这里写你调用接口的基础路径，来解决跨域，如果设置了代理，
  // 那你本地开发环境的axios的baseUrl要写为 '' ，即空字符串
  configureWebpack: {
    // plugins: [
    //   new CopyWebpackPlugin([
    //     {
    //       from: 'src/static',
    //       to: 'static'
    //     }
    //   ])
    // ],
    // resolve: {
    //   alias: {
    //     // key,value自行定义，比如.set('@@', resolve('src/components'))
    //     '/@/': resolve('src'),
    //     '_c': resolve('src/components'),
    //     '_api': resolve('src/api')
    //   }
    // }
  },
  devServer: {
    // run dev 测试环境代理
    host: '0.0.0.0',
    port: '8086',
    hot: true, // 开启热更新
    inline: true,
    open: 'Google Chrome', // 默认使用Google浏览器打开
    clientLogLevel: 'error',
    proxy: 'http://localhost:8086/' // dev环境下，反向代理(数据请求)，解决跨域请求
  }
}
