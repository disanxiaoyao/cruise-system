// import XLSX from 'xlsx'
// import querystring from 'querystring'

const Tools = {

  /**
   * 固化单位排序
   * @param arr 排序前数据
   * @param propName 对象字段名
   * @returns {*} 固化排序后的数据
   */
  sortUnit (arr = [], propName = '') {
    if (arr.length === 0) return
    return arr.sort((a, b) => {
      return unitDist[a[propName]] - unitDist[b[propName]]
    })
  },

  /**
   * @description 实时更新时间和星期
   */
  updateTime () {
    let date = new Date()
    let week = ''
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let hour = date.getHours()
    let mm = date.getMinutes()
    let ss = date.getSeconds()
    month = formatOneToTen(month)
    day = formatOneToTen(day)
    hour = formatOneToTen(hour)
    mm = formatOneToTen(mm)
    ss = formatOneToTen(ss)
    switch (date.getDay()) {
      case 0:
        week = '星期日'
        break
      case 1:
        week = '星期一'
        break
      case 2:
        week = '星期二'
        break
      case 3:
        week = '星期三'
        break
      case 4:
        week = '星期四'
        break
      case 5:
        week = '星期五'
        break
      case 6:
        week = '星期六'
        break
    }
    return year + '年' + month + '月' + day + '日 '
      + hour + ':' + mm + ':' + ss + ' ' + week
  },

  /**
   * @desc 函数防抖（每单位时间内若执行多次则 重头开始执行）
   * @param func 函数
   * @param immediate 默认1 是否立即执行
   *          1 单位时间开始立即执行(立即执行)
   *          2 单位时间结束时执行(执行最后一次)，
   * @param wait 延迟执行毫秒数，默认800ms
   */
  debounce (func, wait = 800, immediate = 2) {
    let timeout
    return function () {
      let context = this
      let args = arguments
      if (timeout) clearTimeout(timeout)
      if (immediate === 1) {
        var callNow = !timeout
        timeout = setTimeout(() => {
          timeout = null
        }, wait)
        if (callNow) func.apply(context, args)
        // if (callNow) func.apply(args)
      } else {
        timeout = setTimeout(function () {
          func.apply(context, args)
        }, wait)
      }
    }
  },

  /**
   * @desc 函数节流（每单位时间内若执行多次则 只执行一次）
   * @param func 函数
   * @param wait 延迟执行毫秒数，默认800ms
   * @param type 默认false ，false 表时间戳版，2 表定时器版
   *  1 时间戳版的函数触发是在时间段内开始的时候，
   *  2 而定时器版的函数触发是在时间段内结束的时候
   */
  throttle (func, wait = 800, type = 1) {
    let previous = 0
    let timeout
    let context = this
    return () => {
      let args = arguments
      if (type === 1) {
        timeout = null
        let now = Date.now()
        if (now - previous > wait) {
          func.apply(context, args)
          previous = now
        }
      } else if (type === 2) {
        previous = null
        if (!timeout) {
          timeout = setTimeout(() => {
            timeout = null
            func.apply(context, args)
          }, wait)
        }
      }
    }
  },

  /**
   * @desc 保留len（默认2）位小数，超出截取，不足补0
   * @param num 数字或者字符串数字
   * @param len 保留长度，默认2
   * @returns {Number,String} 截取长度的数值
   */
  setFloat (num = 0, len = 2) {
    len = len ? parseInt(len) : 0
    let number = 0
    // 字符串转义成数字
    isNaN(num) ? number = num : number = Number(num)
    if (len <= 0) {
      // 直接取整
      return Math.round(number)
    }
    // 四舍五入
    number = Math.round(number * Math.pow(10, len)) / Math.pow(10, len)
    number = Number(number).toFixed(len)
    return number
  },

  /**
   * @desc 前端分页函数
   * @param dataArr 数据源
   * @param pageIndex 当前页，默认为第1页
   * @param pageSize 每页数量，默认每一页10条数据
   * @returns {Array} 分页后的数组
   */
  pageFn (dataArr = [], pageIndex = 1, pageSize = 10) {
    // 保存每页数据数组
    let pageData = []
    let len = dataArr.length
    if (len < 1) {
      console.log('数组长度为0')
      return pageData
    } else {
      // console.log('数组长度为:', len)
      // 设置开始
      let start = (pageIndex * pageSize) - pageSize
      // 设置结束长度
      let end = pageIndex * pageSize
      end = end > dataArr.length ? dataArr.length : end
      for (let i = start; i < end; i++) {
        // 分页处理
        pageData.push(dataArr[i])
      }
    }
    return pageData
  },

  /**
   * @desc 通过对象字段名进行数组去重
   * @param key 对象字段名
   * @param arr 数据源
   * @returns {Array} 去重后的数组
   */
  uniqueFind (arr = [], key = '') {
    let cache = []
    for (let item of arr) {
      if (cache.find(c => c[key] === item[key])) {
        continue
      }
      cache.push(item)
    }
    return cache
  },

  /**
   * @desc 通过属性字段名数值进行数组数据升序排序
   * @param keyName 对象字段名
   * @param arr 数据源
   * @returns {Array} 排序后的数组
   */
  myUpSort (arr = [], keyName = '') {
    let compare = (key) => {
      return (obj1, obj2) => {
        let val1 = Number(obj1[key])
        let val2 = Number(obj2[key])
        return val1 - val2
      }
    }
    return arr.sort(compare(keyName))
  },

  /**
   * 详情表导出
   * filterData:过滤条件(转为url参数)（可选）
   */
  // exportExcel (url, filterData = {}) {
  //   let uri = querystring.stringify(filterData)
  //   if (uri.length) {
  //     url += `?${uri}`
  //   }
  //   const elink = document.createElement('a')
  //   elink.style.display = 'none'
  //   elink.href = process.env.VUE_APP_URL + url
  //   document.body.appendChild(elink)
  //   elink.click()
  //   document.body.removeChild(elink)
  // },

  /**
   * 导出
   */
  // exportXLSX (data, name = 'fileName', titles) {
  // var ws = XLSX.utils.json_to_sheet(data)
  // const range = XLSX.utils.decode_range(ws['!ref'])
  // for (let c = range.s.c; c <= range.e.c; c++) {
  //   const header = XLSX.utils.encode_col(c) + '1'
  //   ws[header].v = titles[ws[header].v]
  // }
  // var wb = XLSX.utils.book_new()
  // XLSX.utils.book_append_sheet(wb, ws, 'People')
  // XLSX.writeFile(wb, name + '.xlsx')
  // },

  /**
   * 保留三位数，添加格式
   */
  numFormater (num) {
    let n = Number(num)
    // console.log(n)
    return (n.toString().indexOf('.') !== -1) ?
      n.toLocaleString() : n.toString()
        .replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')
  },

  unitDist () {
    return unitDist
  }
}
/**
 * 固化排序
 */
const unitDist = {
  福州: 1,
  莆田: 2,
  泉州: 3,
  厦门: 4,
  漳州: 5,
  龙岩: 6,
  三明: 7,
  南平: 8,
  宁德: 9,
  直属单位: 10,
  物资公司: 11
}

/**
 * 将时间低于10的数据前面补零，形如'1'>'01'
 * @param date 传入日期数据
 * @returns {string}
 */
const formatOneToTen = (date = 0) => {
  if (date < 10) {
    date = '0' + date
  }
  return date
}

export default Tools
