import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './lang/i18n'
import 'element-ui/lib/theme-chalk/index.css'
import ElementUI from 'element-ui'
import Viewer from 'v-viewer';
import 'viewerjs/dist/viewer.css';

Vue.use(Viewer);

// Vue.use(ElementUI, {i18n: (key, value) => i18n.t(key, value)})
Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  store,
  render: h => h(App),
}).$mount('#app')
