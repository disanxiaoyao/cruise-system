export default {
  list :[
    {
      "icon": "el-icon-lx-home",
      "index": "homePage",
      "title": "homePage"
    },
    // {
    //   "icon": "el-icon-lx-home",
    //   "index": "i18n",
    //   "title": "i18n"
    // },
    {
      "icon": "el-icon-lx-cascades",
      "index": "intelligentFly",
      "title": "intelligentFly",
      "subs": [
        {
          "index": "flyLive",
          "title": "flyLive"
        },
        {
          "index": "flyTask",
          "title": "flyTask"
        },
        {
          "index": "trackTemplate",
          "title": "trackTemplate"
        },
        {
          "index": "checkManage",
          "title": "checkManage"
        }
      ]
    },
    {
      "icon": "el-icon-lx-sort",
      "index": "lineManage",
      "title": "lineManage",
      "subs": [
        {
          "index": "trackManage",
          "title": "trackManage"
        },
        {
          "index": "lineImport",
          "title": "lineImport"
        },
        {
          "index": "cloudImport",
          "title": "cloudImport"
        }]
    },
    {
      "icon": "el-icon-lx-edit",
      "index": "dataManage",
      "title": "dataManage",
      "subs": [
        {
          "index": "dataImport",
          "title": "dataImport"
        },
        {
          "index": "mapBrowse",
          "title": "mapBrowse"
        },
        {
          "index": "picBrowse",
          "title": "picBrowse"
        },
        {
          "index": "statisticsView",
          "title": "statisticsView"
        }
      ]
    },
    {
      "icon": "el-icon-lx-rank",
      "index": "intelligentAnalysis",
      "title": "intelligentAnalysis",
      "subs": [
        {
          "index": "defectAnalysis",
          "title": "defectAnalysis"
        },
        {
          "index": "defectReview",
          "title": "defectReview"
        },
        {
          "index": "defectReport",
          "title": "defectReport"
        }
      ]
    },
    {
      "icon": "el-icon-lx-info",
      "index": "airplaneManage",
      "title": "airplaneManage",
      "subs": [
        {
          "index": "airplaneId",
          "title": "airplaneId"
        },
        {
          "index": "airplaneReg",
          "title": "airplaneReg"
        },
        {
          "index": "airplaneOut",
          "title": "airplaneOut"
        }
      ]
    },
    {
      "icon": "el-icon-lx-people",
      "index": "peopleManage",
      "title": "peopleManage",
      "subs": [
        {
          "index": "creatTeam",
          "title": "creatTeam"
        },
        {
          "index": "creatPerson",
          "title": "creatPerson"
        },
        {
          "index": "authoritySetting",
          "title": "authoritySetting"
        }
      ]
    }
  ]
}
