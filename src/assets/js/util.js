/* eslint valid-typeof: 0 */
const toString = Object.prototype.toString;
const Util = window.Util || {};

// 类型判断
["Array", "Function", "Object", "RegExp"].forEach((type) => {
    Util[`is${type}`] = (obj) => {
        return obj && toString.call(obj) === `[object ${type}]`;
    };
});

["Boolean", "Number", "String"].forEach((type) => {
    Util[`is${type}`] = (obj) => {
        return typeof obj === type.toLowerCase();
    };
});

// 标准浏览器使用原生的判断方法
if (Array.isArray) {
    Util.isArray = Array.isArray;
}

// 判断是否为空对象
Util.isEmptyObject = (obj) => {
    for (const name in obj) {
        return false;
    }
    return true;
};

// 判断是否为纯粹的对象
Util.isPlainObject = (obj) => {
    if (!obj || !Util.isObject(obj)) {
        return false;
    }

    try {
        for (const name in obj) {
            if (!Object.prototype.hasOwnProperty.call(obj, name)) {
                return false;
            }
        }
    } catch (_) {
        return false;
    }

    return true;
};

// 添加事件监听器
Util.listener = (target, eventType, callback) => {
    let remove = null;
    if (target.addEventListener) {
        target.addEventListener(eventType, callback, false);
        remove = () => target.removeEventListener(eventType, callback, false);
    } else if (target.attachEvent) {
        target.attachEvent(`on${eventType}`, callback);
        remove = () => target.detachEvent(`on${eventType}`, callback);
    }
    return {
        remove
    };
};

// 首字母大写转换
Util.capitalize = (str) => {
    const firstStr = str.charAt(0);
    return firstStr.toUpperCase() + str.replace(firstStr, "");
};

/**
 * 对象深拷贝
 * JSON.parse(JSON.stringify(obj));
 */
Util.clone = (obj) => {
    if (JSON && JSON.parse) {
        return JSON.parse(JSON.stringify(obj));
    }

    let o;
    if (typeof obj === "object") {
        if (obj === null) {
            o = null;
        } else {
            if (obj instanceof Array) {
                o = [];
                for (let i = 0, len = obj.length; i < len; i++) {
                    o.push(Util.clone(obj[i]));
                }
            } else {
                o = {};
                for (const j in obj) {
                    o[j] = Util.clone(obj[j]);
                }
            }
        }
    } else {
        o = obj;
    }

    return o;
};

export default Util;
