// echarts图option参数

// 折线图
export let optionLine = {
  title: {
    // show: false,
    x: 'center',
    y: 'top',
    text: ''
  },
  tooltip: {
    // formatter: '{b}: {c} 天'
    trigger: 'axis'
  },
  legend: {
    // orient: 'vertical',
    top: 24,
    right: 20,
    // selectedMode: false
  },
  color: ['#63B795', '#F3B63F', '#6AA5F8', '#E47470', '#58CD8A'],
  grid: {
    bottom: 0,
    left: 10,
    right: 10,
    containLabel: true
  },
  xAxis: {
    name: '',
    type: 'category',
    data: ['1月', '2月', '3月', '4月',
      '5月', '6月', '7月', '8月',
      '9月', '10月', '11月', '12月'],
    axisLabel: {
      interval: 0,
      rotate: -45
    }
  },
  yAxis: {
    name: '',
    type: 'value',
    splitLine: {
      show: false
    },
    axisLabel: {
      formatter: '{value}'
    }
  },
  series: [
    {
      name: '任务总数',
      data: [11, 2, 5, 22, 34, 11, 33, 22, 9, 10, 11, 12],
      type: 'line',
      smooth: 0.5,
      // barWidth: 18,
      // max: 100,
      // yAxisIndex: 0,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    },
    {
      name: '正执行',
      data: [1, 2, 12, 70, 2, 4, 36, 5, 22, 10, 11, 12],
      type: 'line',
      smooth: 0.5,
      // stack: '1',
      // yAxisIndex: 1,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    },
    {
      name: '完成总数',
      data: [15, 22, 35, 26, 34, 11, 33, 23, 9, 30, 41, 12],
      type: 'line',
      smooth: 0.5,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    },
    {
      name: '缺陷',
      data: [1, 2, 3, 2, 4, 4, 5, 7, 6, 3, 4, 4],
      type: 'line',
      smooth: 0.5,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    },
    {
      name: '正常',
      data: [31, 22, 32, 27, 32, 44, 36, 35, 32, 40, 16, 22],
      type: 'line',
      smooth: 0.5,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    }
  ]
}
export let getOptLine = (obj) => {
  return {
    title: {
      x: 'center',
      y: 'top',
      // text: `全省未验收总条数 {b|${obj.count}} 条，总金额 {a|${obj.money}} 万元`,
      textStyle: {
        formatter: [
          '{a|这段文本采用样式a}',
          '{b|这段文本采用样式b}',
          '{c|这段用样式c}'
        ].join(''),
        rich: {
          a: {
            color: '#E47470',
            fontSize: 24
          },
          b: {
            color: '#6AA5F8',
            fontSize: 24
          },
          c: {
            color: '#FFB300',
            fontSize: 24
          }
        }
      }
    },
    tooltip: {
      // formatter: '{b}: {c} '
    },
    legend: {
      // orient: 'vertical', // horizontal/水平（默认）
      top: 20,
      right: 80,
      data: ['金额', '条目数']
      // selectedMode: false
    },
    grid: {
      bottom: 10,
      left: 20,
      right: 20,
      containLabel: true
    },
    color: ['#E47470', '#6AA5F8', '#F3B63F', '#63B795'],
    xAxis: {
      name: '',
      type: 'category',
      triggerEvent: true,
      data: obj.xData,
      axisLabel: {
        interval: 0,
        formatter: function (val) {
          let strs = val.split('') //字符串数组
          let str = ''
          // eslint-disable-next-line no-cond-assign
          for (let i = 0, s; s = strs[i++];) {
            //遍历字符串数组
            str += s
            if (!(i % 4)) str += '\n'
          }
          return str
        },
        rotate: 0
      }
    },
    yAxis: [
      {
        name: '金额(万元)',
        type: 'value',
        splitLine: {
          show: false
        },
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        name: '数目(条)',
        type: 'value',
        splitLine: {
          show: false
        },
        axisLabel: {
          formatter: '{value}'
        }
      }],
    dataZoom: [
      {
        show: false,
        type: 'slider',
        realtime: true,
        // start: 30,
        // end: 70,
        startValue: obj.startVal, // 数据窗口范围的起始数值
        endValue: obj.endVal
      },
      {
        show: false,
        type: 'inside',
        realtime: true,
        // start: 30,
        // end: 70,
        startValue: 0, // 数据窗口范围的起始数值
        endValue: 8
      }
    ],
    series: [
      {
        name: '金额',
        data: obj.yData1,
        type: 'bar',
        barWidth: 18,
        // barMinHeight: 10,
        label: {
          // show: true,
          position: 'top',
          formatter: '{c}',
          color: '#333333'
        }
      },
      {
        name: '条目数',
        data: obj.yData2,
        type: 'line',
        yAxisIndex: 1, // 从0开始
        label: {
          // show: true,
          position: 'top',
          formatter: '{c}',
          color: '#333333'
        }
      }
    ]
  }
}

// 多数据柱线图
export let optionLineBar = {
  title: {
    // show: false,
    x: 'center',
    y: 'top',
    text: ''
  },
  tooltip: {
    // formatter: '{b}: {c} 天'
  },
  legend: {
    // orient: 'vertical',
    top: 24,
    right: 80,
    data: ['金额', '条目数'],
    // selectedMode: false
  },
  color: ['#63B795', '#F3B63F', '#6AA5F8', '#E47470'],
  grid: {
    bottom: 0,
    left: 0,
    right: 20,
    containLabel: true
  },
  xAxis: {
    name: '',
    type: 'category',
    data: ['福州', '莆田', '泉州', '厦门',
      '漳州', '龙岩', '三明', '南平',
      '宁德', '物资公司', '直属单位', '全省'],
    axisLabel: {
      interval: 0,
      rotate: -45
    }
  },
  yAxis: [
    {
      name: '金额(万元)',
      type: 'value',
      splitLine: {
        show: false
      },
      axisLabel: {
        formatter: '{value}'
      }
    },
    {
      name: '数目(条)',
      type: 'value',
      splitLine: {
        show: false
      },
      axisLabel: {
        formatter: '{value}'
      }
    }],
  series: [
    {
      name: '金额',
      data: [1, 2, 58, 70, 67, 11, 33, 22, 9, 10, 11, 12],
      type: 'line',
      barWidth: 18,
      max: 100,
      yAxisIndex: 0,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    },
    {
      name: '条目数',
      data: [1, 2, 12, 70, 2, 4, 36, 5, 22, 10, 11, 12],
      type: 'bar',
      barWidth: 18,
      // stack: '1',
      yAxisIndex: 1,
      label: {
        show: false,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      }
    }
  ]
}
export let getOptLineBar = (obj) => {
  return {
    title: {
      x: 'center',
      y: 'top',
      // text: `全省未验收总条数 {b|${obj.count}} 条，总金额 {a|${obj.money}} 万元`,
      textStyle: {
        formatter: [
          '{a|这段文本采用样式a}',
          '{b|这段文本采用样式b}',
          '{c|这段用样式c}'
        ].join(''),
        rich: {
          a: {
            color: '#E47470',
            fontSize: 24
          },
          b: {
            color: '#6AA5F8',
            fontSize: 24
          },
          c: {
            color: '#FFB300',
            fontSize: 24
          }
        }
      }
    },
    tooltip: {
      // formatter: '{b}: {c} '
    },
    legend: {
      // orient: 'vertical', // horizontal/水平（默认）
      top: 20,
      right: 80,
      data: ['金额', '条目数']
      // selectedMode: false
    },
    grid: {
      bottom: 10,
      left: 20,
      right: 20,
      containLabel: true
    },
    color: ['#E47470', '#6AA5F8', '#F3B63F', '#63B795'],
    xAxis: {
      name: '',
      type: 'category',
      triggerEvent: true,
      data: obj.xData,
      axisLabel: {
        interval: 0,
        formatter: function (val) {
          let strs = val.split('') //字符串数组
          let str = ''
          // eslint-disable-next-line no-cond-assign
          for (let i = 0, s; s = strs[i++];) {
            //遍历字符串数组
            str += s
            if (!(i % 4)) str += '\n'
          }
          return str
        },
        rotate: 0
      }
    },
    yAxis: [
      {
        name: '金额(万元)',
        type: 'value',
        splitLine: {
          show: false
        },
        axisLabel: {
          formatter: '{value}'
        }
      },
      {
        name: '数目(条)',
        type: 'value',
        splitLine: {
          show: false
        },
        axisLabel: {
          formatter: '{value}'
        }
      }],
    dataZoom: [
      {
        show: false,
        type: 'slider',
        realtime: true,
        // start: 30,
        // end: 70,
        startValue: obj.startVal, // 数据窗口范围的起始数值
        endValue: obj.endVal
      },
      {
        show: false,
        type: 'inside',
        realtime: true,
        // start: 30,
        // end: 70,
        startValue: 0, // 数据窗口范围的起始数值
        endValue: 8
      }
    ],
    series: [
      {
        name: '金额',
        data: obj.yData1,
        type: 'bar',
        barWidth: 18,
        // barMinHeight: 10,
        label: {
          // show: true,
          position: 'top',
          formatter: '{c}',
          color: '#333333'
        }
      },
      {
        name: '条目数',
        data: obj.yData2,
        type: 'line',
        yAxisIndex: 1, // 从0开始
        label: {
          // show: true,
          position: 'top',
          formatter: '{c}',
          color: '#333333'
        }
      }
    ]
  }
}

// 饼图
export let optionPie = {
  title: {
    x: 'center',
    y: 'top',
    text: ''
  },
  grid: {
    top: 30,
    bottom: 4,
    left: 0,
    right: 0,
    containLabel: true
  },
  legend: {
    orient: 'vertical', // horizontal/水平（默认）
    top: 0,
    right: 0,
    selectedMode: false
  },
  tooltip: {
    // formatter: '{b}: {c}条 ({d}%)'
  },
  color: ['#5C85F8', '#F8C452', '#25B9E3', '#58CD8A', '#F67070'],
  series: [
    {
      name: '',
      type: 'pie',
      center: ['50%', '60%'],
      radius: '70%',
      avoidLabelOverlap: false,
      startAngle: 180,
      itemStyle: {
        borderRadius: 10,
        borderColor: '#fff',
        borderWidth: 2
      },
      label: {
        // show: false,
        position: 'outline',
        formatter: function (params) { // 指示线对应文字
          return params.name + '\n'
            + params.value + ' ('
            + params.percent + '%)'
        }
      },
      emphasis: {
        label: {
          show: true,
          fontSize: '20',
          fontWeight: 'bold'
        }
      },
      data: [
        {value: 1048, name: '普通缺陷'},
        {value: 735, name: '紧急缺陷'},
        {value: 580, name: '严重缺陷'},
        {value: 484, name: '一般缺陷'}
      ]
    }
  ]
}
export let getOptPie = (arr) => {
  return {
    title: {
      x: 'center',
      y: 'top',
      text: '长期未执行合同超期时长统计'
    },
    grid: {
      top: 30,
      bottom: 4,
      left: 0,
      right: 0,
      containLabel: true
    },
    legend: {
      orient: 'vertical', // horizontal/水平（默认）
      top: 0,
      right: 0,
      data: ['1~1.5年', '1.5~2年', '2年以上'],
      selectedMode: false
    },
    tooltip: {
      formatter: '{b}: {c}条 ({d}%)'
    },
    color: ['#63B795', '#6AA5F8', '#F3B63F', '#E47470'],
    series: [
      {
        type: 'pie',
        radius: '55%',
        startAngle: 180,
        center: ['50%', '64%'],
        avoidLabelOverlap: true,
        itemStyle: {
          normal: {
            label: { // 此处为指示线文字
              show: true,
              // position: 'outside', // 标签的位置
              textStyle: {
                fontWeight: 200
                // fontSize: 10    //文字的字体大小
              },
              formatter: function (params) { // 指示线对应文字
                return params.name + '\n'
                  + params.value + ' 条('
                  + params.percent + '%)'
              }
            },
            labelLine: {    //指示线状态
              show: true,
              smooth: 0.2,
              length: 10,
              length2: 20
            }
          }
        },
        data: arr
      }]
  }
}

// 环图
export let optionRing = {
  title: {
    x: 'center',
    y: 'top',
    text: ''
  },
  grid: {
    top: 30,
    bottom: 4,
    left: 0,
    right: 0,
    containLabel: true
  },
  legend: {
    orient: 'vertical', // horizontal/水平（默认）
    top: 0,
    right: 0,
    selectedMode: false
  },
  tooltip: {
    formatter: '{b}: {c} ({d}%)'
  },
  color: ['#5C85F8', '#F8C452', '#25B9E3', '#58CD8A', '#F67070'],
  series: [
    {
      name: '',
      type: 'pie',
      radius: ['40%', '70%'],
      center: ['50%', '60%'],
      avoidLabelOverlap: true,
      startAngle: 180,
      itemStyle: {
        borderRadius: 10,
        borderColor: '#fff',
        borderWidth: 2
      },
      label: {
        // show: false,
        position: 'outline',
        formatter: function (params) { // 指示线对应文字
          return params.name + '\n'
            + params.value + ' ('
            + params.percent + '%)'
        }
      },
      emphasis: {
        label: {
          show: true,
          fontSize: '20',
          fontWeight: 'bold'
        }
      },
      labelLine: {
        show: true,
        length: 20,
        lineStyle: {
          // color: 各异,
          width: 1,
          type: 'solid'
        }
      },
      data: [
        {value: 448, name: '杆塔'},
        {value: 735, name: '电池'},
        {value: 580, name: '出勤'},
        {value: 484, name: '无人机'},
        {value: 300, name: '汽车'}
      ]
    }
  ]
}
export let getOptRing = (arr) => {
  return {
    title: {
      x: 'center',
      y: 'top',
      text: '长期未执行合同超期时长统计'
    },
    grid: {
      top: 30,
      bottom: 4,
      left: 0,
      right: 0,
      containLabel: true
    },
    legend: {
      orient: 'vertical', // horizontal/水平（默认）
      top: 0,
      right: 0,
      data: ['1~1.5年', '1.5~2年', '2年以上'],
      selectedMode: false
    },
    tooltip: {
      formatter: '{b}: {c}条 ({d}%)'
    },
    color: ['#63B795', '#6AA5F8', '#F3B63F', '#E47470'],
    series: [
      {
        type: 'pie',
        radius: '55%',
        startAngle: 180,
        center: ['50%', '64%'],
        avoidLabelOverlap: true,
        itemStyle: {
          normal: {
            label: { // 此处为指示线文字
              show: true,
              // position: 'outside', // 标签的位置
              textStyle: {
                fontWeight: 200
                // fontSize: 10    //文字的字体大小
              },
              // formatter: function (params) { // 指示线对应文字
              //   return params.name + '\n'
              //     + params.value + ' ('
              //     + params.percent + '%)'
              // }
            },
            labelLine: {    //指示线状态
              show: true,
              smooth: 0.2,
              length: 10,
              length2: 20
            }
          }
        },
        data: arr
      }]
  }
}

// 柱状图
export let optionBar = {
  title: {
    x: 'center',
    y: 'top',
    text: ''
  },
  tooltip: {},
  legend: {
    orient: 'vertical',
    top: 0,
    right: 0,
    data: ['份数'],
    selectedMode: false
  },
  grid: {
    bottom: 0,
    left: 0,
    right: 20,
    containLabel: true
  },
  color: ['#5C85F8'],
  xAxis: {
    type: 'category',
    data: ['飞行架次', '飞行距离', '飞行时间', '损耗'],
    axisLabel: {
      interval: 0,
      // rotate: -45,
      formatter: function (val) {
        let strs = val.split('') //字符串数组
        let str = ''
        // eslint-disable-next-line no-cond-assign
        for (let i = 0, s; s = strs[i++];) { //遍历字符串数组
          str += s
          if (!(i % 4)) str += '\n'
        }
        return str
      }
    }
  },
  yAxis: {
    name: '',
    type: 'value',
    splitLine: {
      // show: false
    },
    // max: 100,
    nameTextStyle: {
      align: 'left'
    },
    axisLabel: {
      formatter: '{value}'
    }

  },
  series: [
    {
      name: '',
      data: [76, 66, 63, 67],
      type: 'bar',
      barWidth: 30,
      label: {
        show: true,
        position: 'top',
        formatter: '{c}',
        color: '#333333'
      },
      itemStyle: {
        emphasis: {
          color: '#aec3fb'
        }
      }
    }
  ]
}
export let getOptBar = (obj) => {
  return {
    title: {
      x: 'center',
      y: 'top',
      // text:`全省未到货订单总金额{a|${obj.count}}万元`,
      text: '长期未执行合同份数物资小类TOP5',
      textStyle: {
        formatter: [
          '{a|这段文本采用样式a}',
          '{b|这段文本采用样式b}',
          '{c|这段用样式c}'
        ].join(''),
        rich: {
          a: {
            color: '#FFB300',
            fontSize: 24
          },
          b: {
            color: '#F56C6C',
            fontSize: 24
          },
          c: {
            color: '#FFB300',
            fontSize: 24
          },
        }
      }
    },
    tooltip: {},
    legend: {
      orient: 'vertical',
      top: 0,
      right: 0,
      data: ['物资小类TOP5'],
      selectedMode: false
    },
    grid: {
      bottom: 10,
      left: 10,
      right: 20,
      containLabel: true
    },
    color: ['#F3B63F'],
    xAxis: {
      type: 'category',
      data: obj.xData,
      axisLabel: {
        interval: 0,
        formatter: function (val) {
          let strs = val.split('') //字符串数组
          let str = ''
          // eslint-disable-next-line no-cond-assign
          for (let i = 0, s; s = strs[i++];) { //遍历字符串数组
            str += s
            if (!(i % 4)) str += '\n'
          }
          return str
        },
        rotate: 0
      }
    },
    yAxis: {
      name: '物资小类TOP5',
      type: 'value',
      splitLine: {
        show: false
      },
      nameTextStyle: {
        align: 'left'
      }
    },
    series: [
      {
        name: '物资小类TOP5',
        data: obj.yData,
        type: 'bar',
        barWidth: 30,
        // barMinHeight: 10,
        label: {
          show: true,
          position: 'top',
          formatter: '{c}',
          color: '#333333'
        }
      }
    ]
  }
}
