
// string                            ------> {{$t("i18n.name")}}
// placeholder of input's attributes ------> :placeholder="$t('i18n.name')"
// code                              ------> this.$t("i18n.name")

export default {
    // menus
    authoritySetting: 'Permissions',
    creatPerson: 'Create staff',
    creatTeam: 'Create crews',
    peopleManage: 'Team management',
    airplaneOut: 'Aircraft Outbound',
    airplaneReg: 'Aircraft register',
    airplaneId: 'Aircraft Model',
    airplaneManage: 'Aircraft management',
    defectReport: 'Defect report',
    defectReview: 'Defect review',
    defectAnalysis: 'Defect analysis',
    intelligentAnalysis: 'Defect management',
    statisticsView: 'Data Chart',
    picBrowse: 'Data Photo',
    mapBrowse: 'Data Map',
    dataImport: 'Data Import',
    dataManage: 'Data management',
    cloudImport: 'Pointcloud import',
    lineImport: 'Route import',
    trackManage: 'Route List',
    lineManage: 'Route management',
    checkManage: 'Flight Inspection',
    trackTemplate: 'Track template',
    flyTask: 'Fly task',
    flyLive: 'Flight streaming',
    intelligentFly: 'Flight management',
    homePage: 'Workstation',
    // title
    userTitle: 'Power Intelligent Inspection System',
}
