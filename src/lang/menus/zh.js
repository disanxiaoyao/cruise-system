
// 一般文字模板 ------------------------> {{$t("i18n.name")}}
// input输入框中的placeholder属性 ------> :placeholder="$t('i18n.name')"
// 代码中 ---------------------------->  this.$t("i18n.name")

export default {
    // 菜单
    authoritySetting: '权限设置',
    creatPerson: '创建人员',
    creatTeam: '创建班组',
    peopleManage: '人员管理',
    airplaneOut: '出库管理',
    airplaneReg: '飞机注册',
    airplaneId: '型号管理',
    airplaneManage: '飞机管理',
    defectReport: '缺陷报告',
    defectReview: '缺陷审核',
    defectAnalysis: '缺陷分析',
    intelligentAnalysis: '缺陷管理',
    statisticsView: '统计视图',
    picBrowse: '图片浏览',
    mapBrowse: '地图浏览',
    dataImport: '导入数据',
    dataManage: '数据管理',
    cloudImport: '点云导入',
    lineImport: '线路导入',
    trackManage: '航迹管理',
    lineManage: '线路管理',
    checkManage: '检点管理',
    trackTemplate: '航线模板',
    flyTask: '飞行任务',
    flyLive: '飞行直播',
    intelligentFly: '智能飞行',
    homePage: '首页工作台',
    // 标题
    userTitle: '电力智能巡检系统',
}
