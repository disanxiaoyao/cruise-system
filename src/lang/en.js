
import menus from './menus/en'
import homePage from './homePage/en'
import login from './loginPage/en'

export default {
  i18n: {
    ...menus,
    ...login,
    ...homePage,
    // i18n page test
    breadcrumb: 'International Products',
    tips: 'Click on the button to change the current language. ',
    btn: 'Switch Chinese',
    title1: 'Common usage',
    p1: 'If you reveal your secrets to the wind you should not blame the wind for  revealing them to the trees.',
    p2: 'Nothing can help us endure dark times better than our faith. ',
    p3: 'If you can do what you do best and be happy, you\'re further along in life  than most people.'
  }
}

// string                            ------> {{$t("i18n.name")}}
// placeholder of input's attributes ------> :placeholder="$t('i18n.name')"
// code                              ------> this.$t("i18n.name")

