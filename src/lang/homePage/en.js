
// string                            ------> {{$t("i18n.name")}}
// placeholder of input's attributes ------> :placeholder="$t('i18n.name')"
// code                              ------> this.$t("i18n.name")

export default {
    admin: 'admin',
    defectType: 'Type of Defect Statistics',
    dataStatistics: 'Data Statistics',
    workStatistics: 'Work statistics',
    planeStatistics: 'Plane statistics',
    planeManagement: 'Plane management',
    searchKeyWord: 'Key word for search',
    work: 'Work',
    details: 'Details',
    flightOverview: 'Flight overview'
}

