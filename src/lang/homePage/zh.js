
// 一般文字模板 ------------------------> {{$t("i18n.name")}}
// input输入框中的placeholder属性 ------> :placeholder="$t('i18n.name')"
// 代码中 ---------------------------->  this.$t("i18n.name")

export default {
    admin: '系统管理员',
    defectType: '缺陷分类统计',
    dataStatistics: '数据统计',
    workStatistics: '任务统计',
    planeStatistics: '飞机统计',
    planeManagement: '飞机管理',
    searchKeyWord: '输入关键词搜索',
    work: '我的工作',
    details: '查看详情',
    flightOverview: '飞行概况'
}
