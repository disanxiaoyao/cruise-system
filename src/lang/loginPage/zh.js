
// 一般文字模板 ------------------------> {{$t("i18n.name")}}
// input输入框中的placeholder属性 ------> :placeholder="$t('i18n.name')"
// 代码中 ---------------------------->  this.$t("i18n.name")

export default {
    tip: 'Tips : 用户名和密码随便填。',
    login: '登入',
    username: '请输入账号',
    password: '请输入密码',
    welcome: '欢迎登陆',
    title: '电力智能巡检系统'
}
