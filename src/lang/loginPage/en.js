
// string                            ------> {{$t("i18n.name")}}
// placeholder of input's attributes ------> :placeholder="$t('i18n.name')"
// code                              ------> this.$t("i18n.name")

export default {
    tip: 'Tips : Just fill in your username and password',
    login: 'login',
    username: 'username',
    password: 'password',
    welcome: 'Welcome To',
    title: 'Power Intelligent Inspection System'
}
